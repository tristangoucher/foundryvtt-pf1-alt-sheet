import { NoteEditor } from "../apps/note-editor.mjs";
import { SettingsEditor } from "../apps/settings.mjs";
import { skillAlwaysShown, getSkipActionPrompt } from "./helpers.mjs";

/*
In a "perfect" world, I would use a mixin here:
const AltSheetMixin = (BaseClass) => class extends BaseClass { ... };
class AltActorSheetPFCharacter extends AltSheetMixin(ActorSheetPFCharacter) {};
class AltActorSheetPFNPC extends AltSheetMixin(ActorSheetPFNPC) {};

That way, we would have no code duplication here.
And no Object.assign();
However, this breaks event bubbling to the base classes BUT ONLY IN BROWSERS!
Desktop electron foundry is fine.

Revisit this in a year or whenever.
*/
export class AltActorSheetPFCharacter extends pf1.applications.actor.ActorSheetPFCharacter {
  get template() {
    // TODO: change this to our own lite sheet
    if (!game.user.isGM && this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "modules/pf1-alt-sheet/templates/altsheet.hbs";
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["pf1alt", "sheet", "actor", "character"],
      width: 820,
      height: 840,
      scrollY: [
        ".buffs-body",
        ".feats-body",
        ".spells_primary-body",
        ".spells_secondary-body",
        ".spells_tertiary-body",
        ".spells_spelllike-body",
        ".skills-list.adventure",
        ".skills-list.background",
        ".combat-attacks",
        ".inventory-body",
        ".attributes-body",
      ],
      tabs: [
        {
          navSelector: "nav.tabs[data-group='primary']",
          contentSelector: "section.primary-body",
          initial: "summary",
          group: "primary",
        },
        {
          navSelector: "nav.tabs[data-group='skillset']",
          contentSelector: "section.skillset-body",
          initial: "adventure",
          group: "skills",
        },
        {
          navSelector: "nav.tabs[data-group='spellbooks']",
          contentSelector: "section.spellbooks-body",
          initial: "primary",
          group: "spellbooks",
        },
      ],
    });
  }

  get currentPrimaryTab() {
    const primaryElem = this.element.find('nav[data-group="primary"] .item.active');
    if (primaryElem.length !== 1) return null;
    return primaryElem.attr("data-tab");
  }

  async getData() {
    return this.mixinGetData(await super.getData());
  }

  activateListeners(html) {
    super.activateListeners(html);
    this.mixinActivateListeners(html);
  }
}

// Yes, code duplication. Because JS.
export class AltActorSheetPFNPC extends pf1.applications.actor.ActorSheetPFNPC {
  get template() {
    // TODO: change this to our own lite sheet
    if (!game.user.isGM && this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "modules/pf1-alt-sheet/templates/altsheet.hbs";
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["pf1alt", "sheet", "actor", "character"],
      width: 820,
      height: 840,
      scrollY: [
        ".buffs-body",
        ".feats-body",
        ".spells_primary-body",
        ".spells_secondary-body",
        ".spells_tertiary-body",
        ".spells_spelllike-body",
        ".skills-list.adventure",
        ".skills-list.background",
        ".combat-attacks",
        ".inventory-body",
        ".attributes-body",
      ],
      tabs: [
        {
          navSelector: "nav.tabs[data-group='primary']",
          contentSelector: "section.primary-body",
          initial: "summary",
        },
        {
          navSelector: "nav.tabs[data-group='skillset']",
          contentSelector: "section.skillset-body",
          initial: "adventure",
        },
        {
          navSelector: "nav.tabs[data-group='spellbooks']",
          contentSelector: "section.spellbooks-body",
          initial: "primary",
        },
      ],
    });
  }

  get currentPrimaryTab() {
    const primaryElem = this.element.find('nav[data-group="primary"] .item.active');
    if (primaryElem.length !== 1) return null;
    return primaryElem.attr("data-tab");
  }

  async getData() {
    return await this.mixinGetData(await super.getData());
  }

  activateListeners(html) {
    super.activateListeners(html);
    this.mixinActivateListeners(html);
  }
}

// Define shared functions here.
// DO NOT USE super, IT DOES NOT WORK AND I CAN'T USE A MIXIN CLASS BECAUSE REASONS!
export const AltSheetMixin = {
  defaultConfig() {
    return {
      headerSidebar: false,
      sidebarOptions: {
        false: "PF1.No",
        true: "PF1.Yes",
      },
      skills: {},
      features: {
        minimizeClasses: false,
      },
      links: {
        mode: "none",
        options: {
          none: game.i18n.localize("PF1AS.Links.ModeNone"),
          familiar: game.i18n.localize("PF1AS.Links.ModeFamiliar"),
        },
      },
    };
  },

  getModuleActorConfig() {
    // Check if there is a config for this actor
    let config = this.actor.getFlag("pf1-alt-sheet", "config");
    if (config === undefined) {
      config = this.defaultConfig();
    }

    if (config.headerSidebar === true || config.headerSidebar === "true") config.headerSidebar = "true";
    else config.headerSidebar = "false";

    if (!config.theme) config.theme = "theme-default";

    config.links.options = {
      none: game.i18n.localize("PF1AS.Links.ModeNone"),
      familiar: game.i18n.localize("PF1AS.Links.ModeFamiliar"),
    };

    config.sidebarOptions = {
      false: game.i18n.localize("PF1.No"),
      true: game.i18n.localize("PF1.Yes"),
    };

    config.themeOptions = Object.fromEntries(
      Object.values(CONFIG.altSheet.themes).map((theme) => [theme.cssClass, game.i18n.localize(theme.label)])
    );

    return config;
  },

  resetModuleActorConfig() {
    console.log("pf1-alt-sheet | Resetting actor config");
    this.actor.unsetFlag("pf1-alt-sheet", "config");
  },

  setModuleActorConfig(update) {
    const context = foundry.utils.mergeObject(this.getModuleActorConfig(), update, { inplace: false });
    delete context.links.options;
    delete context.sidebarOptions;
    this.actor.setFlag("pf1-alt-sheet", "config", context);
  },

  setLinkOptions(options) {
    this.setModuleActorConfig({
      links: {
        mode: options.mode,
      },
    });
  },

  setSidebarHeader(value) {
    this.setModuleActorConfig({
      headerSidebar: value,
    });
  },

  setTheme(value) {
    this.setModuleActorConfig({
      theme: value,
    });
  },

  blendColors(color1, color2, percentage) {
    color1 = color1.replace("#", "");
    color2 = color2.replace("#", "");
    percentage /= 100;

    // Parse the colors into RGB components
    const r1 = parseInt(color1.substring(0, 2), 16);
    const g1 = parseInt(color1.substring(2, 4), 16);
    const b1 = parseInt(color1.substring(4, 6), 16);

    const r2 = parseInt(color2.substring(0, 2), 16);
    const g2 = parseInt(color2.substring(2, 4), 16);
    const b2 = parseInt(color2.substring(4, 6), 16);

    // Calculate the blended color
    const r = Math.round(r1 + (r2 - r1) * percentage)
      .toString(16)
      .padStart(2, "0");
    const g = Math.round(g1 + (g2 - g1) * percentage)
      .toString(16)
      .padStart(2, "0");
    const b = Math.round(b1 + (b2 - b1) * percentage)
      .toString(16)
      .padStart(2, "0");

    // Return the blended color as a hex string
    return `#${r}${g}${b}`;
  },

  async mixinGetData(context) {
    context.encumbrance.colors = {
      light: this.blendColors("#2281cb", "#c0365a", context.encumbrance.pct.light),
      medium: this.blendColors("#2281cb", "#c0365a", context.encumbrance.pct.medium),
      heavy: this.blendColors("#2281cb", "#c0365a", context.encumbrance.pct.heavy),
    };

    const al = game.modules.get("koboldworks-pf1-actor-link");
    const actorLinkEnabled = al && al.active;

    // inject some stuff for our sheet
    // Compendium entry for Natural Attacks
    const naturalAttacks = context.attacks.find((a) => a.id === "natural");
    naturalAttacks.compendiumId = "pf1.monster-abilities";

    // Attack can be broken
    const weaponAttacks = context.attacks.find((a) => a.id === "weaponLike");
    if (weaponAttacks) weaponAttacks.canBreak = true;

    // Weapons and Equipment can be broken
    const inventoryWeapons = context.inventory.find((a) => a.id === "weapon");
    if (inventoryWeapons) inventoryWeapons.canBreak = true;
    const inventoryEquipment = context.inventory.find((a) => a.id === "equipment");
    if (inventoryEquipment) inventoryEquipment.canBreak = true;

    context = foundry.utils.mergeObject(
      context,
      {
        system: {
          abilities: {
            str: { labelShort: game.i18n.localize("PF1.AbilityShortStr") },
            dex: { labelShort: game.i18n.localize("PF1.AbilityShortDex") },
            con: { labelShort: game.i18n.localize("PF1.AbilityShortCon") },
            int: { labelShort: game.i18n.localize("PF1.AbilityShortInt") },
            wis: { labelShort: game.i18n.localize("PF1.AbilityShortWis") },
            cha: { labelShort: game.i18n.localize("PF1.AbilityShortCha") },
          },
          attributes: {
            ac: {
              normal: { labelShort: game.i18n.localize("PF1AS.ACShort") },
              touch: { labelShort: game.i18n.localize("PF1AS.TouchShort") },
              flatFooted: { labelShort: game.i18n.localize("PF1AS.FFShort") },
            },
          },
        },
        skillsets: {
          known: { skills: {} },
        },
        altSheet: this.getModuleActorConfig(),
        actorLinkEnabled: actorLinkEnabled,
      },
      { inplace: false, insertValues: true, overwrite: false, recursive: true }
    );

    // add hpMax to each class and prep class associations
    if (context.classes.length && context.classes[0].items) {
      for (const clazz of context.classes[0].items) {
        clazz.hpMax = clazz.hd * clazz.level;

        if (!clazz.links) clazz.links = { classAssociations: [] };
        if (!clazz.links.classAssociations) clazz.links.classAssociation = [];
        clazz.links.classAssociations = clazz.links.classAssociations.map((a) => {
          const linkItem = fromUuidSync(a.uuid);
          if (linkItem) {
            a.name = linkItem.name;
            a.img = linkItem.img;
          }

          return a;
        });
      }
    }

    const filters = context.filters.sections.features;
    const filterMinSize = (filters?.has("prepared") ? 1 : 0) + (filters?.has("empty") ? 1 : 0);
    // Inject classes so that the filters include classes for the features
    context.features.unshift({
      browse: { type: "class" },
      category: "features",
      create: { type: "class", system: { subType: "base" } },
      filters: [{ type: "class" }],
      id: "class",
      interface: { create: true, level: true, types: true },
      label: game.i18n.localize("PF1.Classes"),
      path: "classes.class",
      items: context.classes.length ? context.classes[0].items : [],
      isClass: true,
      _hideEmpty: filters?.has("empty"),
      _preparedOnly: filters?.has("prepared"),
      _hidden: filters?.size > filterMinSize && !filters?.has("class"),
    });

    // build the "known skill" list for the first page of the sheet, taken from 3.5e
    const skillkeys = Object.keys(context.skillsets.all.skills).sort(function (a, b) {
      if (context.skillsets.all.skills[a].custom && !context.skillsets.all.skills[b].custom) return 1;
      if (!context.skillsets.all.skills[a].custom && context.skillsets.all.skills[b].custom) return -1;
      return ("" + context.skillsets.all.skills[a].label).localeCompare(context.skillsets.all.skills[b].label);
    });
    skillkeys.forEach((a) => {
      const skl = context.skillsets.all.skills[a];
      context.skillsets.all.skills[a].alwaysShown = skillAlwaysShown(context, a, undefined);
      if (skl.rank > 0 || skillAlwaysShown(context, a, undefined)) {
        context.skillsets.known.skills[a] = skl;
      } else if (skl.subSkills !== undefined) {
        context.skillsets.known.skills[a] = skl;
      }
    });

    // actor link familiar mode for skills
    if (actorLinkEnabled) {
      context = await this.doActorLinkStuff(context);
    }

    this.actorData = context;

    return context;
  },

  async doActorLinkStuff(context) {
    // Check if this actor is linked to another
    const linked = context.rollData._linked;
    if (!linked) {
      return context;
    }
    const modSkillsets = {
      skillsets: {
        all: {
          skills: {},
        },
      },
    };
    // Check if mode is familiar
    if (context.altSheet.links.mode === "familiar") {
      // Inject master ranks into skills
      for (const [name, skill] of Object.entries(context.skillsets.all.skills)) {
        modSkillsets.skillsets.all.skills[name] = {};
        modSkillsets.skillsets.all.skills[name].masterRank = linked.skills[name].rank;
        let clb = (skill.rank ?? 0) == 0 && linked.skills[name].rank > 0 && skill.cs ? 3 : 0;
        modSkillsets.skillsets.all.skills[name].masterMod =
          skill.mod - (skill.rank ?? 0) + (linked.skills[name].rank ?? 0) + clb;
        if (!context.skillsets.all.skills[name].subSkills) {
          continue;
        }
        for (const [subName, subSkill] of Object.entries(context.skillsets.all.skills[name].subSkills)) {
          if (subName in linked.skills[name].subSkills) {
            if (!("subSkills" in modSkillsets.skillsets.all.skills[name])) {
              modSkillsets.skillsets.all.skills[name].subSkills = {};
            }
            modSkillsets.skillsets.all.skills[name].subSkills[subName] = {};
            modSkillsets.skillsets.all.skills[name].subSkills[subName].masterRank =
              linked.skills[name].subSkills[subName].rank;
            clb = (subSkill.rank ?? 0) == 0 && linked.skills[name].subSkills[subName].rank > 0 && skill.cs ? 3 : 0;
            modSkillsets.skillsets.all.skills[name].subSkills[subName].masterMod =
              subSkill.mod - (subSkill.rank ?? 0) + (linked.skills[name].subSkills[subName].rank ?? 0) + clb;
          }
        }
      }
      context = foundry.utils.mergeObject(context, modSkillsets);
    }

    return context;
  },

  changesMod(sourceArr) {
    let total = 0;
    sourceArr.forEach((c) => {
      total += c.value;
    });
    return total;
  },

  mixinActivateListeners(html) {
    html.find(".note-editor").click(this._onNoteEditor.bind(this));

    html.find(".settings-button").click(this._onSettings.bind(this));

    html.find(".spell-slots-per-level span.text-box").on("click", (event) => {
      this._onSpanTextInput(event, this._onSubmit.bind(this));
    });

    // Remove handler added by default sheet
    html.find(".item .item-name").off("click");
    html.find(".item .item-name").off("contextmenu");

    // Re-add them for the correct node
    html.find(".item .item-name .item-name-text").contextmenu(this._onItemEdit.bind(this));
    html.find(".item .item-name .item-name-text").click((event) => this._onItemSummary(event));

    html.find(".skill > .skill-mod-total > .rollable").click(this._onRollSkillCheck.bind(this));
    html.find(".sub-skill > .skill-mod-total > .rollable").click(this._onRollSkillCheck.bind(this));

    html.find(".skill > .skill-name > .rollable").click(this._onRollSkillCheck.bind(this));
    html.find(".sub-skill > .skill-name > .rollable").click(this._onRollSkillCheck.bind(this));

    html.find(".skill > .skill-master-mod-total > .rollable").click(this._onRollMasterSkillCheck.bind(this));
    html.find(".sub-skill > .skill-master-mod-total > .rollable").click(this._onRollMasterSubSkillCheck.bind(this));

    html.find(".altsheet-skill-checkbox").click(this._onAltSheetCheckbox.bind(this));
    html.find(".altsheet-minimize-classes-checkbox").click(this._onAltSheetCheckbox.bind(this));

    // Edit skill
    html.find(".skills .skill > .skill-details > .controls .skill-edit").click((ev) => this._onSkillEdit(ev));
    // Delete custom skill
    html.find(".skills .skill > .skill-details > .controls .skill-delete").click((ev) => this._onSkillDelete(ev));

    html.find(".spellcasting-concentration .rollable").click(this._onRollConcentration.bind(this));
    html.find(".spellcasting-cl .rollable").click(this._onRollCL.bind(this));

    html.find(".tab.buffs .conditions-down").click((_event) => {
      $(".tab.buffs .conditions").slideToggle();
      $(".tab.buffs .conditions-up").toggle();
      $(".tab.buffs .conditions-down").toggle();
    });
    html.find(".tab.buffs .conditions-up").click((_event) => {
      $(".tab.buffs .conditions").slideToggle();
      $(".tab.buffs .conditions-up").toggle();
      $(".tab.buffs .conditions-down").toggle();
    });
  },

  _onAltSheetCheckbox(event) {
    event.preventDefault();
    event.stopPropagation();
    const path = event.target.attributes["data-altsheet-config-path"].value;
    const currentValue = foundry.utils.getProperty(this.getModuleActorConfig(), path);
    const newData = {};
    foundry.utils.setProperty(newData, path, !currentValue);
    this.setModuleActorConfig(newData);
  },

  _onNoteEditor(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const options = {
      name: a.getAttribute("for"),
      title: a.getAttribute("title"),
      fields: a.dataset.fields,
      dtypes: a.dataset.dtypes,
      theme: this.getModuleActorConfig().theme,
    };
    new NoteEditor(this.actor, options).render(true);
  },

  _onSettings(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const options = {
      name: "settings",
      title: game.i18n.localize("PF1.Settings"),
      fields: a.dataset.fields,
      dtypes: a.dataset.dtypes,
      theme: this.getModuleActorConfig().theme,
    };
    new SettingsEditor(this, options).render(true);
  },

  async _onRollMasterSkillCheck(event) {
    event.preventDefault();
    const skill = event.currentTarget.parentElement.parentElement.dataset.skill;
    const originalRanks = this.document.system.skills[skill].rank;
    this.document.system.skills[skill].rank = this.document.rollData._linked.skills[skill].rank;
    await this.document.rollSkill(skill, { event: event, skipDialog: getSkipActionPrompt() });
    this.document.system.skills[skill].rank = originalRanks;
  },

  async _onRollMasterSubSkillCheck(event) {
    event.preventDefault();
    const mainSkill = event.currentTarget.parentElement.parentElement.dataset.mainSkill;
    const skill = event.currentTarget.parentElement.parentElement.dataset.skill;
    const originalRanks = this.document.system.skills[mainSkill].subSkills[skill].rank;
    this.document.system.skills[mainSkill].subSkills[skill].rank =
      this.document.data.rollData._linked.skills[mainSkill].subSkills[skill].rank;
    await this.rollSkill(`${mainSkill}.subSkills.${skill}`, { event: event, skipDialog: getSkipActionPrompt() });
    this.document.system.skills[mainSkill].subSkills[skill].rank = originalRanks;
  },
};
