/**
 * Add chat hooks for the PF1 Alt Sheet
 */
export function addChatHooks() {
  // eslint-disable-next-line no-undef
  libWrapper.register("pf1-alt-sheet", "pf1.actionUse.ChatAttack.prototype.addDamage", addDamageWrapper, "WRAPPER");

  Hooks.on("pf1PreDisplayActionUse", (actionUse) => {
    actionUse.shared.chatTemplate = "modules/pf1-alt-sheet/templates/chat/attack-roll.hbs";
  });
}

/**
 * Add damage to the chat card
 *
 * @param {Function} fnext  The next function in the chain
 * @param {object} options  The options object
 */
async function addDamageWrapper(fnext, options = {}) {
  await fnext(options);

  let data = this.damage;
  if (options.critical === true) data = this.critDamage;

  // Determine total damage
  let totalDamage = data.total;

  // Handle minimum damage rule
  let minimumDamage = false;
  if (totalDamage < 1) {
    totalDamage = 1;
    minimumDamage = true;
  }

  // New cards
  if (options.critical) {
    this.cards.critical = {};
    this.cards.critical.items = [];
    if (this.isHealing) {
      this.cards.critical.items.push({
        label: "0.5x",
        value: -Math.floor(totalDamage / 2),
        action: "applyDamage",
      });
      this.cards.critical.items.push({
        label: "1x",
        value: -totalDamage,
        action: "applyDamage",
      });
      this.cards.critical.items.push({
        label: "1.5x",
        value: -Math.floor(totalDamage * 1.5),
        action: "applyDamage",
      });
    } else {
      this.cards.critical.items.push({
        label: "0.5x",
        value: Math.floor(totalDamage / 2),
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
      this.cards.critical.items.push({
        label: "1x",
        value: totalDamage,
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
      this.cards.critical.items.push({
        label: "1.5x",
        value: Math.floor(totalDamage * 1.5),
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
    }
  } else {
    this.cards.damage = {};
    this.cards.damage.items = [];
    if (this.isHealing) {
      this.cards.damage.items.push({
        label: "0.5x",
        value: -Math.floor(totalDamage / 2),
        action: "applyDamage",
      });
      this.cards.damage.items.push({
        label: "1x",
        value: -totalDamage,
        action: "applyDamage",
      });
      this.cards.damage.items.push({
        label: "1.5x",
        value: -Math.floor(totalDamage * 1.5),
        action: "applyDamage",
      });
    } else {
      this.cards.damage.items.push({
        label: "0.5x",
        value: Math.floor(totalDamage / 2),
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
      this.cards.damage.items.push({
        label: "1x",
        value: totalDamage,
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
      this.cards.damage.items.push({
        label: "1.5x",
        value: Math.floor(totalDamage * 1.5),
        action: "applyDamage",
        tags: minimumDamage ? "nonlethal" : "",
      });
    }
  }
}
