/**
 * Handle tooltips for the alt sheet with the PF1E system
 *
 * @param {object} sheet   The sheet object
 * @param {string} fullId  The full ID of the tooltip
 * @param {object} context   The context object
 * @param {Function} wrapped   The wrapped function
 * @returns   {Promise<void>}  A Promise that resolves when the tooltips have been handled
 */
export async function handleTooltips(sheet, fullId, context, wrapped) {
  const actor = sheet.actor,
    system = actor.system;

  let header, subHeader;
  const details = [];
  const paths = [];
  const sources = [];
  let notes;

  const re = /^(?<id>[\w-]+)(?:\.(?<detail>.*))?$/.exec(fullId);
  const { id, detail } = re?.groups ?? {};

  let useSystemFunction = false;
  switch (id) {
    case "conditionImmunity":
      paths.push({ path: "@traits.ci.total", empty: true });
      paths.push({ path: `${Array.from(system.traits.ci.names).join(", ")}`, empty: true });
      break;
    case "damageImmunity":
      paths.push({ path: "@traits.di.total", empty: true });
      paths.push({ path: `${Array.from(system.traits.di.names).join(", ")}`, empty: true });
      break;
    case "damageVulnerability":
      paths.push({ path: "@traits.dv.total", empty: true });
      paths.push({ path: `${Array.from(system.traits.dv.names).join(", ")}`, empty: true });
      break;
    case "spellbook": {
      const [bookId, target, subTarget] = detail.split(".");
      const spellbook = system.attributes?.spells?.spellbooks?.[bookId];
      switch (target) {
        case "concentrationDCs": {
          header = game.i18n.localize("PF1AS.Spells.NeededConcentration");
          const baseDC = spellbook.concentration?.total;
          const formulas = {
            defensive: 15 + 2 * +subTarget,
            entangled: 15 + +subTarget,
            vigorousMotion: 10 + +subTarget,
            violentMotion: 15 + +subTarget,
            extremeMotion: 20 + +subTarget,
            violentWeather1: 5 + +subTarget,
            violentWeather2: 10 + +subTarget,
          };

          paths.push(
            {
              path: game.i18n.localize("PF1AS.Spells.Defensive"),
              value:
                baseDC + 1 > formulas.defensive
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.defensive,
            },
            {
              path: game.i18n.localize("PF1.Condition.entangled"),
              value:
                baseDC + 1 > formulas.entangled
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.entangled,
            },
            {
              path: game.i18n.localize("PF1AS.Spells.VigorousMotion"),
              value:
                baseDC + 1 > formulas.vigorousMotion
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.vigorousMotion,
            },
            {
              path: game.i18n.localize("PF1AS.Spells.ViolentMotion"),
              value:
                baseDC + 1 > formulas.violentMotion
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.violentMotion,
            },
            {
              path: game.i18n.localize("PF1AS.Spells.ExtremeMotion"),
              value:
                baseDC + 1 > formulas.extremeMotion
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.extremeMotion,
            },
            {
              path: game.i18n.localize("PF1AS.Spells.ViolentWeather1"),
              value:
                baseDC + 1 > formulas.violentWeather1
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.violentWeather1,
            },
            {
              path: game.i18n.localize("PF1AS.Spells.ViolentWeather2"),
              value:
                baseDC + 1 > formulas.violentWeather2
                  ? game.i18n.localize("PF1AS.Spells.AlwaysSucceeds")
                  : formulas.violentWeather2,
            }
          );

          break;
        }
        default: {
          useSystemFunction = true;
          break;
        }
      }
      break;
    }
    default: {
      useSystemFunction = true;
      break;
    }
  }

  if (useSystemFunction) {
    return await wrapped(fullId, context);
  } else {
    context.header = header;
    context.subHeader = subHeader;
    context.details = details;
    context.paths = paths;
    context.sources = sources;
    context.notes = notes ?? [];
  }
}
