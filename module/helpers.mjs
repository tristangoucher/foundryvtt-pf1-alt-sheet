/**
 * Helper functions for the PF1 Alt Sheet
 */
export function registerHandlebarsHelpers() {
  Handlebars.registerHelper("isMetric", () => {
    return game.settings.get("pf1", "units") === "metric";
  });

  Handlebars.registerHelper("abilityDamaged", (abl) => {
    return abl.damage > 0 || abl.drain > 0 || abl.penalty > 0;
  });

  Handlebars.registerHelper("toMetric", (ft) => {
    return (ft * 3.0) / 10.0;
  });

  Handlebars.registerHelper("replaceNewLines", (text) => {
    return text.replace(/\n/g, "<br />");
  });

  Handlebars.registerHelper("isPositive", (value) => {
    return value > 0;
  });

  Handlebars.registerHelper("hasBaseSpellSlots", (obj, level) => {
    if (obj === undefined) return false;
    return obj?.spells[`spell${level}`]?.base != null;
  });

  Handlebars.registerHelper("isMythicPath", (clazz) => {
    return clazz.subType === "mythic";
  });

  Handlebars.registerHelper("isNegative", (value) => {
    return value < 0;
  });

  Handlebars.registerHelper("skillAlwaysShown", skillAlwaysShown);

  Handlebars.registerHelper("subskillShownInSummary", (actor, skill, subskill) => {
    return actor.skillsets.all.skills[skill].subSkills[subskill].rank > 0 || skillAlwaysShown(actor, skill, subskill);
  });

  Handlebars.registerHelper("neededConcentrationDefensive", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 15 + section.level * 2)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${15 + section.level * 2}`;
  });

  Handlebars.registerHelper("neededConcentrationEntangled", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 15 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${15 + section.level}`;
  });

  Handlebars.registerHelper("neededConcentrationVigorousMotion", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 10 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${10 + section.level}`;
  });

  Handlebars.registerHelper("neededConcentrationViolentMotion", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 15 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${15 + section.level}`;
  });

  Handlebars.registerHelper("neededConcentrationExtremeMotion", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 20 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${20 + section.level}`;
  });
  Handlebars.registerHelper("neededConcentrationViolentWeather1", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 5 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${5 + section.level}`;
  });
  Handlebars.registerHelper("neededConcentrationViolentWeather2", (spellbook, section) => {
    if (spellbook.concentration.total + 1 >= 10 + section.level)
      return game.i18n.localize("PF1AS.Spells.AlwaysSucceeds");
    return `${10 + section.level}`;
  });
}

/**
 * Check if a skill or subskill should always be shown in the skill list
 *
 * @param {object} actor  The actor object
 * @param {string} skill  The skill
 * @param {string} subskill   The subskill
 * @returns   {boolean}   True if the skill should always be shown
 */
export function skillAlwaysShown(actor, skill, subskill) {
  if (!actor?.altSheet) {
    console.log("pf1-alt-sheet | altSheet not present in actor, this should not happen");
    return false;
  }
  if (!actor.altSheet.skills) {
    console.log("pf1-alt-sheet | skills not present in actor.altSheet, this should not happen");
    return false;
  }
  if (!actor.altSheet.skills[skill]) {
    actor.altSheet.skills[skill] = {
      as: false,
    };
  }
  if (subskill !== undefined) {
    if (!("subskills" in actor.altSheet.skills[skill])) {
      actor.altSheet.skills[skill].subskills = {};
    }
    if (!(subskill in actor.altSheet.skills[skill].subskills)) {
      actor.altSheet.skills[skill].subskills[subskill] = { as: false };
    }
    return actor.altSheet.skills[skill].subskills[subskill].as;
  }
  return actor.altSheet.skills[skill].as;
}

/**
 * Register the sheet fields with AIP
 */
export function registerWithAIP() {
  const aip = game.modules.get("autocomplete-inline-properties");
  if (aip && aip.active) {
    const DATA_MODE = aip.API.CONST.DATA_MODE;
    const sheetFields = [
      {
        selector: `.tab[data-tab="attributes"] .attribute.nac input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="attributes"] .attribute.sr input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="feats"] .features-formula input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="skills"] .skill-rank-formula input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="spellbook"] .spellcasting-misc input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="spellbook"] .spellbook-info-box .formulas input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
      {
        selector: `.tab[data-tab="spellbook"] .spellbook-info-box .spell-slot-ability-bonus input[type="text"]`,
        showButton: true,
        allowHotkey: true,
        dataMode: DATA_MODE.ROLL_DATA,
      },
    ];
    const aipConfig = {
      packageName: "pf1-alt-sheet",
      sheetClasses: [
        {
          name: "AltActorSheetPFCharacter",
          fieldConfigs: sheetFields,
        },
        {
          name: "AltActorSheetPFNPC",
          fieldConfigs: sheetFields,
        },
      ],
    };
    aip.API.PACKAGE_CONFIG.push(aipConfig);
  }
}

/**
 * Register the module settings
 */
export function registerSettings() {
  game.settings.register("pf1-alt-sheet", "addAttackChatCardTemplate", {
    name: "PF1AS.Chat.addAttackChatCardTemplateN",
    hint: "PF1AS.Chat.addAttackChatCardTemplateH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: (val) => {
      if (val === false) {
        game.settings.set("pf1", "attackChatCardTemplate", "systems/pf1/templates/chat/attack-roll.hbs");
      }
      window.location.reload();
    },
  });
}

/**
 * Check if the action prompt should be skipped
 *
 * @returns {boolean}   True if the action prompt should be skipped
 */
export function getSkipActionPrompt() {
  return (
    (game.settings.get("pf1", "skipActionDialogs") && !game.keyboard.downKeys.has("Shift")) ||
    (!game.settings.get("pf1", "skipActionDialogs") && game.keyboard.downKeys.has("Shift"))
  );
}
