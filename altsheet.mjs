import { registerHandlebarsHelpers, registerWithAIP, registerSettings } from "./module/helpers.mjs";
import { addChatHooks } from "./module/chat.mjs";
import { AltSheetMixin, AltActorSheetPFCharacter, AltActorSheetPFNPC } from "./module/sheets.mjs";
import { handleTooltips } from "./module/tooltips.mjs";

Hooks.once("init", () => {
  registerSettings();

  const templates = [
    "modules/pf1-alt-sheet/templates/altsheet.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-attacks.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-attributes.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-buffs.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-details.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-features.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-header.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-inventory.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-skills-front.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-skills.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-spellbook-front.hbs",
    "modules/pf1-alt-sheet/templates/parts/actor-spellbook.hbs",
    "modules/pf1-alt-sheet/templates/chat/damage-type.hbs",
  ];
  loadTemplates(templates);

  registerHandlebarsHelpers();

  registerWithAIP();

  if (game.settings.get("pf1-alt-sheet", "addAttackChatCardTemplate") === true) {
    addChatHooks();
  }

  CONFIG.fontDefinitions["NewRocker"] = {
    editor: true,
    fonts: [{ urls: ["modules/pf1-alt-sheet/fonts/Newrocker-Gy5a.ttf"] }],
  };

  CONFIG.fontDefinitions["ModernAntiqua"] = {
    editor: true,
    fonts: [{ urls: ["modules/pf1-alt-sheet/fonts/ModernAntiqua-Regular.ttf"] }],
  };

  CONFIG.fontDefinitions["EagleLake"] = {
    editor: true,
    fonts: [{ urls: ["modules/pf1-alt-sheet/fonts/EagleLake-Regular.ttf"] }],
  };

  CONFIG.fontDefinitions["Medieval Sharp Regular"] = {
    editor: true,
    fonts: [{ urls: ["modules/pf1-alt-sheet/fonts/MedievalSharp-Regular.ttf"] }],
  };

  CONFIG.altSheet = {
    themes: {
      default: {
        label: "PF1AS.Themes.Default",
        cssClass: "theme-default",
      },
      pathfinder: {
        label: "PF1AS.Themes.Pathfinder",
        cssClass: "theme-pathfinder",
      },
      riseotrl: {
        label: "PF1AS.Themes.Riseotrl",
        cssClass: "theme-riseotrl",
      },
    },
  };

  console.log("pf1-alt-sheet | loaded");
});

Hooks.on("ready", async () => {
  if (!game.modules.get("lib-wrapper")?.active && game.user.isGM) {
    ui.notifications.error("Module pf1-alt-sheet requires the 'libWrapper' module. Please install and activate it.");
    return;
  }

  Object.assign(AltActorSheetPFCharacter.prototype, AltSheetMixin);
  DocumentSheetConfig.registerSheet(Actor, "pf1alt", AltActorSheetPFCharacter, {
    label: "PF1AS.CharacterSheetLabel",
    types: ["character"],
    makeDefault: false,
  });

  Object.assign(AltActorSheetPFNPC.prototype, AltSheetMixin);
  DocumentSheetConfig.registerSheet(Actor, "pf1alt", AltActorSheetPFNPC, {
    label: "PF1AS.NPCSheetLabel",
    types: ["npc"],
    makeDefault: false,
  });

  DocumentSheetConfig.updateDefaultSheets();

  console.log("pf1-alt-sheet | sheets registered");
});

Hooks.once("libWrapper.Ready", () => {
  // eslint-disable-next-line no-undef
  libWrapper.register(
    "pf1-alt-sheet",
    "pf1.applications.actor.ActorSheetPF.prototype._getTooltipContext",
    async function (wrapped, fullId, context) {
      return await handleTooltips(this, fullId, context, wrapped);
    },
    "MIXED"
  );
});
