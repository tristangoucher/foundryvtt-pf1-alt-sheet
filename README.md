# Alternate character sheet for Pathfinder 1e

This is an alternative character sheet for the Pathfinder 1e system on FoundryVTT.

It is a combination of the 3.5 SRD sheet (the old PF1 sheet) and the current PF1 sheet with some custom modifications.

This sheet might not have features that the default sheet has.
If you think something essential is missing, please submit an issue here on GitLab.

## Manifest

Manifest for Foundry installation: https://gitlab.com/Tryss_Farron/foundryvtt-pf1-alt-sheet/-/raw/master/module.json

## Known Issues

- None

## Theming

While I will be working to expand the sheet with multiple themes over time, it is possible to use Mana's Custom CSS module to alter the CSS variables that PF1 Alt Sheet uses.

In v11.1.0, I've added theme support to the sheet. The theme can be configured for each actor invidividually by changing the theme in the sheet's settings. The selection of themes is currently limited to:

- Default (mostly monochrome/greyscale)
- Pathfinder (colorized based off of common PF1E book colors)
- Rise of the Runelords (colorized based on the Rise of the Runelords AP book)

With that limited selection in mind, here's a reproduced version of how to do so with the current styling:

```CSS
.pf1alt.sheet,
.pf1alt.app {
  &.theme-default,
  .theme-default {
    --pf1alt-attribute-header-bg: rgb(189 189 189 / 30%);
    --pf1alt-attribute-header-color: #000;
    --pf1alt-attribute-content-bg: rgb(189 189 189 / 15%);
    --pf1alt-rollable-color: #5c9bab;
    --pf1alt-rollable-control-color: #306b79cc;
    --pf1alt-rollable-control-color-disabled: #2c606d38;
    --pf1alt-rollable-color-disabled: #2c606d38;
    --pf1alt-rollable-hover-color: #2c606d;
    --pf1alt-input-color: #2c606d;
    --pf1alt-hover-shadow: #ed0000;
    --pf1alt-border-color: #d1d1d1;
    --pf1alt-select-option-color: #dad8cc;
    --pf1alt-img-border-color-inverted: #4c62e1;
    --pf1alt-line-shading-odd: rgba(210, 210, 210, 0.3);
    --pf1alt-line-shading-even: rgba(155, 155, 155, 0.3);
    --pf1alt-gradient-shade1: rgb(186 186 167 / 15%);
    --pf1alt-gradient-shade2: rgb(186 186 167 / 15%);
    --pf1alt-filter-border-color: #8a8983;
    --pf1alt-item-header-bg: rgb(189 189 189 / 30%);
    --pf1alt-nav-border-color: #75514b;
    --pf1alt-nav-border-active-color: #44191a;
    --pf1alt-nav-hover-shade: rgb(122 122 122 / 80%);
    --pf1alt-nav-active-shade: rgb(0 0 0 / 35%);
    --pf1alt-encumbered-bg: #44191a;
    --pf1alt-encumbered-border-color: #be4a4c;
    --pf1alt-encumbrance-bar-bg: #6c8aa5;
    --pf1alt-encumbrance-bar-border-color: #cde4ff;
    --pf1alt-negative-skill-mod: #44191a;
    --pf1alt-buttonbg: rgba(0, 0, 0, 0.1);
    --pf1alt-buttoncolor: #000;
    --pf1alt-panel-bg: rgb(189 189 189 / 30%);
    --pf1alt-header-bg: rgb(0 0 0 / 80%);
    --pf1alt-item-name-hover-color: #4c62e1;
    --pf1alt-font-color: #000;
    --pf1alt-font-color-muted: rgb(0 0 0 / 75%);
    --pf1alt-xpbar-color: #afebff;
    --pf1alt-header-border-color: rgb(127 127 127);
    --pf1alt-default-font-color: #191813;
    --pf1alt-default-border-color: #b5b3a4;
    --pf1alt-buttonbg-active: rgba(0, 0, 0, 0.45);
    --pf1alt-tag-color: rgb(185 185 185 / 80%);
    --pf1alt-condition-active: rgb(138 138 138 / 50%);
    --pf1alt-checkbox-color: #666;
    --pf1alt-disabled-input-color: #4b4a44;
    --pf1alt-label-list-color: #4b4a44;
    --pf1alt-charname-color: #193f48;
    --pf1alt-rollable-header-hover-color: #01bfff;
    --pf1alt-summary-bg: rgb(0 0 0 / 10%);
  }
}
```

The sheets also use a selection of fonts which are currenly shared by all themes:

```CSS
.pf1alt.sheet,
.pf1alt.app {
  --pf1alt-simple-text-font: "Signika", sans-serif;
  --pf1alt-sheet-nav-font: "Modesto Condensed Bold", "Signika", serif;
  --pf1alt-charname-font: "NewRocker", "Signika", serif;
  --pf1alt-sheet-header-font: "ModernAntiquaNumbers", "ModernAntiqua", "Signika", serif;
  --pf1alt-sheet-header-simple-font: "ModernAntiquaNumbers", "ModernAntiqua", "Nodesto", "Signika", serif;
  --pf1alt-sheet-section-font: "ModernAntiquaNumbers", "EagleLake", "Signika", "Palatino Linotype", serif;
  --pf1alt-ability-header-font: "ModernAntiquaNumbers", "ModernAntiqua", "Nodesto", "Signika", "Palatino Linotype", serif;
  --pf1alt-block-header-font: "ModernAntiquaNumbers", "EagleLake", "Signika", serif;
  --pf1alt-ability-score-font: "ModernAntiquaNumbers", "Nodesto", "Signika", serif;
  --pf1alt-attribute-font: "ModernAntiquaNumbers", "EagleLake", "Signika", serif;
}
```

### Adding Additional Themes

The Alt Sheet stores its available themes in `CONFIG.altSheet.themes`, a structure that looks like this:

```JS
CONFIG.altSheet = {
    themes: {
      default: {
        label: "PF1AS.Themes.Default",
        cssClass: "theme-default",
      }
    }
};
```

In this structure, the `label` will be shown in the Theme selection in the sheet's settings. This can be a translation string that the sheet will try to translate OR just any labelling you want to use. The `cssClass` is what will be added to the UI to denote using your custom theming.

## Links

[Pathfinder 1e system](https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1)

[3.5 SRD system](https://github.com/Rughalt/D35E)

[New Rocker font](https://www.fontspace.com/newrocker-font-f15707)

## Pics

![First page](img/front.png)

![Attributes](img/attributes.png)

![Combat](img/combat.png)

![Features](img/features.png)

![Skills](img/skills.png)

![Spells](img/spells.png)
