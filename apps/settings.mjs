export class SettingsEditor extends FormApplication {
  constructor(...args) {
    super(...args);

    this.sheet = this.object;
    this.options.classes.push(args[1].theme);
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "settings-editor",
      classes: ["pf1alt", "entry", "settings-editor"],
      title: game.i18n.localize("PF1.Settings"),
      template: "modules/pf1-alt-sheet/templates/apps/settings.hbs",
      width: 600,
      height: 600,
      closeOnSubmit: true,
      submitOnClose: false,
    });
  }

  get attribute() {
    return this.options.name;
  }

  getData() {
    return this.sheet.actorData;
  }

  async _updateObject(event, formData) {
    if (event.target.name === "reset-config") {
      // Delete altsheet config.
      this.sheet.resetModuleActorConfig();
    }

    const linkOptions = {
      mode: formData["altSheet.links.mode"],
    };
    this.sheet.setLinkOptions(linkOptions);
    if (linkOptions.mode) {
      await this.sheet.actor.setFlag(
        "koboldworks-pf1-actor-link",
        "actorLink.familiar",
        linkOptions.mode === "familiar"
      );
    }

    if (formData["altSheet.headerSidebar"]) {
      this.sheet.setSidebarHeader(formData["altSheet.headerSidebar"] === "true");
    }

    if (formData["altSheet.theme"]) {
      this.sheet.setTheme(formData["altSheet.theme"]);
    }

    formData = foundry.utils.expandObject(formData);
    delete formData.altSheet;

    return await this.sheet.actor.update(formData);
  }
}
