export class NoteEditor extends FormApplication {
  constructor(...args) {
    super(...args);

    this.noteData = foundry.utils.duplicate(foundry.utils.getProperty(this.object, this.attribute) || "");
    this.options.classes.push(args[1].theme);
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "note-editor",
      classes: ["pf1alt", "entry", "note-editor"],
      title: "Notes Selector",
      template: "modules/pf1-alt-sheet/templates/apps/note-editor.hbs",
      width: 400,
      height: 400,
      closeOnSubmit: true,
      submitOnClose: false,
    });
  }

  get attribute() {
    return this.options.name;
  }

  getData() {
    return { noteData: this.noteData };
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find("textarea").change(this._onEntryChange.bind(this));
  }

  async _onEntryChange(event) {
    const a = event.currentTarget;
    this.noteData = a.value;
  }

  async _updateObject(_event, _formData) {
    const updateData = {};

    updateData[this.attribute] = this.noteData;

    return await this.object.update(updateData);
  }
}
