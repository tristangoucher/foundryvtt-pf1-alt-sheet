# 11.1.0

- Fixed class creation and browsing interactions
- Fixed spell uses display hiding the casts/slots left
- Fixed spontaneous caster spell preparation
- Fixed subskill creation/ordering in skills tab
- Fixed skill control interactions not being functional
- Fixed font applications in various contexts
- Add Theme support

# 11.0.2

- Fixed alignment of Race placeholder if the actor didn't have a race
- Fixed alignment of CR and XP info in the sidebar
- Fixed an issue with creating new skills

# 11.0.1

- Fixed instability with sidebar setting
- Internal: Simplified Settings and Note Editor apps
- Fixed spellbook assignment for drag-n-dropping spells onto the actor sheet

# 11.0.0

- Fix compatibility with PF1E v11
- Reformatted remaining tooltips to use the system's processes
- Created a colorized theme for the actor sheet, notes editor, and settings window
- Added an alternate side view of the sheet header, controlled in the sheet settings

# 10.0.0

- Fix compatibility with PF1E v10
- Fix missing ammo recovery on chat cards
- Improved support for Little Helper integration
- Improved support for Blind Rolls for Pathfinder 1e
- Migrated tooltips for the most part to use the system's processes

# 0.9.6

- Fix broken expanding of actor items
- Fix spell not being able to be (un)prepared when using spell points
- Fix checkbox for spontaneous showing when using spell points

# 0.9.5

- Fix class features summary not being able to be opened
- Fix features not showing action activation type
- Fix custom damage types not always being shown in chat card

# 0.9.4

- pf1 v9.3 compat

# 0.9.3

- pf1 v0.82.5 compat

# 0.9.0 - 0.9.2

- Foundry v10 and pf1 v0.82 compat

# 0.8.3

- Remove ellipsis helper usage
- Show - instead of NaN on summary page for abilities without a value
- PF 0.81.3 compat

# 0.8.2

- Rename warning css class
- Fix module.json

# 0.8.1

- Fix wrapping when an attack has multiple damage types
- Fix breakage when attacks have no action

# 0.8.0

- pf1 v0.81.0 compat
- Add spell concentration tooltips to spell level headers
- Add buff durations to buffs table
- Use skill edit menu from default sheet

# 0.7.8

- Fix spellbooks not being disableable from sheet settings
- Fix empty spellbook labels in sheet settings
- Fix broken concentration base value in tooltip that does not exist anymore
- Implement stack splitting like the default sheet

# 0.7.7

- Fix pf1 0.80.22 compat (senses support). Fixes #36

# 0.7.6

- Fix pf1 0.80.15 compat

# 0.7.5

- Fix item tags in inventory not showing when hovering over the resources
- Show use icon for prepared spells that have been used
- Fix compat with pf1 v0.80.10
- Fix alt chat card button widths

# 0.7.4

- Fix checks in skillAlwaysShown

# 0.7.3

- Fix arbitrary skill addition and deletion

# 0.7.2

- Fix alternate chat card roll details on click
- Add target display to alt chat card

# 0.7.1

- Fix spellbook toggling with pf1 0.80.7

# 0.7.0

- Fix skill rolling in pf1 0.80+
- Compat with Foundry v9

# 0.6.2

- Fix display of three-digit speeds
- Fix no spell description in alternate chat cards provided by this module
- Show disabled features as disabled

# 0.6.1

- Fix rolling concentration and caster level checks

# 0.6.0

- Required PF1 version update
- Fix CMB display being double the value
- Fix carry capacity input field

# 0.5.2

- Fix nonlethal damage apply in the chat card not working

# 0.5.1

- Improve chat card a bit
- Fixed at-will spells not having a use action button (thanks websterguy)
- Fix CMB display

# 0.5.0

- Added alternative attack chat card style
- Fix some condition compendium links
- Update attack and encumbrance tooltips to match the default sheet

# 0.4.5

- Added shortcut to buff browser on conditions tab (thanks Fair Strides)

# 0.4.4

- Added Spanish translation (thanks Wharomaru Zhamal)

# 0.4.3

- Added some checks to prevent some sheet not rendering issues

# 0.4.2

- Potentially fix setting as default sheet not working
- Add Italian translation (thanks Davide Mascaretti)

# 0.4.1

- Fix feature charges tooltip showing no item tag.
- Show class HP and possible max HP in features tab
- Fix alt sheet config updates not force a sheet render
- Update settings window to match pf1 settings
- Made base skills rollable

# 0.4.0

- Added interaction with [Actor Link](https://gitlab.com/koboldworks/pf1/actor-link).
  - When the sheet is set to Actor Link Mode Familiar, it will make it possible to roll with the ranks of the parent actor on the familiar.
  - Known issue: the actor sheet needs to be opened twice before the master ranks are shown.
  - Known issue: changing the master will not update the familiar, the sheet has to be reopened first.

# 0.3.3

- Fix an error with the AIP integration when AIP was installed but not active
- Fix charged quick actions not displaying charges
- Show warning when too many feats are on an actor
- Show warning when too many skill ranks are used by an actor
- Re-add compendium links for conditions

# 0.3.2

- Fix some layouting with CN locale
- Update AIP API to v2
- Implement item list filtering via search box like default sheet

# v0.3.1

- Use the conditions display/toggle-thingy from the default sheet

# v0.3.0

- Compat with Foundry v0.8.6 and PF1 >v0.78.0
- Moved spellslot progression radio buttons up

# v0.2.4

- Added a tooltip to the caster level in spellbooks to show the roll + bonus
- Autoconvert the speed to metric. This forces you to enter the speed in ft even when using metric.
  - There is a tooltip explaining that you need to do this, but there is no automatic migration.
  - This is done to do the same as the default sheet because the converted value is now provided to Drag Ruler.
- Added a checkbox to the class list to switch the layout to something more compact.
- Use a different CSS class for the instructions in the features tab to improve compatibility with Koboldworks
- Fix charges not being shown on the summary tab.
- Fix compatibility with PF1 0.77.22, especially the new spell slot system.
  - Warning: Some things might be missing/not working yet!
